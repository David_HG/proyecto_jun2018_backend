console.log("Hola Mundo")
var express = require('express');
var CryptoJS = require("crypto-js");
var app = express();
var port = process.env.PORT|| 3000;

//Para rellenar el Body vacío//
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMlabURL = "https://api.mlab.com/api/1/databases/mlab_sfr/collections/";
var mlabApkikey = "apiKey=wAoGePQuuckkWWi0Q3xbsR9DKjWjJrJo";
var requestJson =  require('request-json');

app.listen(port);

console.log("API Molona escuchando en el puerto" + port);

app.get ("/apitechu/v1",
  function(req,res) {
    // Aconsejabe poner en el log, el método y la ruta pàra saber que ejecuta//
    console.log("GET /apitechu/v1");
    res.send({"msg": "Respuesta desde  Apitechu"});

  }
);

//LOGIN CON MLAB//
app.put("/apitechu/v2/login",
function(req, res){
  console.log("put /apitechu/v2/login");
  console.log("Email is: " + req.body.Email);
  console.log("Password encrypted is: " +  req.body.Password);

  // Decrypt
  var bytes  = CryptoJS.AES.decrypt(req.body.Password.toString(), 'secret_pass');
  var pass = bytes.toString(CryptoJS.enc.Utf8);
  console.log("Password decrypted is: " + pass);

  var newUser = {
    "Email": req.body.Email,
    "Password" : pass
  };

  var_msg = {
    "mensaje":"Login Incorrecto"
      }

      var query =  'q={"Email":"' + newUser.Email + '", "Password":"' + newUser.Password +'"}';
      //var putBody =  '{"$set": {"logged":true}}';//
      var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
  //ar users  = require('./usuarios.json'); //es una array////se reogen los emails y passwords fichero.json en users//



          function(err, resMlab, body) {
              //err:400 es un error del servidor//
              //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
              //body , arrary de la tabla//

              //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
              //var response = !err ? body : {
              //  "msg": "Error obteniendo usuario."

              var response = {};

                if (err) {
                  response = {
                        "msg": "Error obtendiendo el usuario"
                  }
                  res.status(500);

                } else {

                    if (body.length > 0){
                        //response =body;
                        var putBody = '{"$set":{"logged": true}}'
                        httpClient.put("Usuarios?" + query + "&" + mlabApkikey, JSON.parse(putBody));

                        response ={
                            "msg": "Usuario encontrado",
                            "id": body[0].id,
                            "Avatar" : body[0].Avatar
                        };
                        res.send(response);
                      } else {
                        response ={
                            "msg": "Error  usuario no encontrado"
                      };
                      res.status(404);
                    }
              }
          }
  )
}
);



//LOGOUT CON MLAB//
//
//
app.put("/apitechu/v2/logout",
function(req, res){
  console.log("POST /apitechu/v2/logout");
  console.log("id is: " + req.body.id);
  console.log(req.body);

  var newUser = {
    "id": req.body.id
  };

  var_msg = {
    "mensaje":"Login Incorrecto"
      }

      var query =  'q={"id":' + newUser.id + '}';
      var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
  //ar users  = require('./usuarios.json'); //es una array////se reogen los emails y passwords fichero.json en users//



          function(err, resMlab, body) {
              //err:400 es un error del servidor//
              //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
              //body , arrary de la tabla//

              //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
              //var response = !err ? body : {
              //  "msg": "Error obteniendo usuario."

              var response = {};

                if (err) {
                  response = {
                        "msg": "Error obtendiendo el usuario"
                  }
                  res.status(500);

                } else {
                    if (body.length > 0){
                        //response =body;
                        var putBody = '{"$unset":{"logged": ""}}'
                        httpClient.put("Usuarios?" + query + "&" + mlabApkikey, JSON.parse(putBody));

                        response ={
                            "msg": "Log out Correcto",
                            "idUsuario": body[0].id
                        };

                      } else {
                        response ={
                            "msg": "Error  usuario no encontrado"
                      };
                      res.status(404);
                    }

              }
            res.send(response);
          }

  )
}
);

// GET CONTRA LA BASE DE DATOS V2 OBTENER TODOS LOS USUARIOS//
app.get("/apitechu/v2/Usuarios",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios");

    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("Usuarios?" + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo usuarios"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error usuarios no encontrados"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);

// GET CONTRA LA BASE DE DATOS V2 OBTENER EL USUARIO POR ID//
app.get("/apitechu/v2/Usuarios/:id",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios/:id");

    // se recoge la id//
    var id = req.params.id;
    //se crea la consulta//
    var query =  'q={"id":' + id + '}';


    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");


    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo el usuario"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error  usuario no encontrado"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);

// GET CONTRA LA BASE DE DATOS V2 OBTENER LAS CUENTAS POR ID//
app.get("/apitechu/v2/Usuarios/:id/Cuentas",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios/:id/Cuentas");

    // se recoge la id//
    var id = req.params.id;
    //se crea la consulta//
    var query =  'q={"id":' + id + '}';
    console.log(id);
    console.log(query);

    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");


    httpClient.get("Cuentas?" + query + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Toda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo el usuario"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error  usuario no encontrado"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);


// GET CONTRA LA BASE DE DATOS V2 OBTENER LOS MOVIMIENTOS DE LAS CUENTAS POR USUARIO//
 app.get("/apitechu/v2/Usuarios/:id/:idCuenta/Cuentas",
  function(req, res){
    console.log("GET /apitechu/v2/Usuarios/:id/:idCuenta/Cuentas");

    // se recoge la id//

      var id = req.params.id;
      var idCuenta=req.params.idCuenta;



   console.log(idCuenta);
    //se crea la consulta//

    //+ ',' + '{' "idCuenta:" + id_idCuenta.idCuenta + '}'
    var query =  'q={"id":' + id + '},' + '{' + "idCuenta:" + idCuenta + '}';
    console.log(id);
    console.log(idCuenta);
    console.log(query);

    var httpClient= requestJson.createClient(baseMlabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("Cuentas?" + query + "&" + mlabApkikey,
            function(err, resMlab, body) {
                //err:400 es un error del servidor//
                //resMla, Toda la respuesta que monta el cliente con las cabeceras y Body
                //body , arrary de la tabla//

                //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                //var response = !err ? body : {
                //  "msg": "Error obteniendo usuario."

                var response = {};

                  if (err) {
                    response = {
                          "msg": "Error obtendiendo el usuario"
                    }
                    res.status(500);

                  } else {
                      if (body.length > 0){
                          response =body;
                        } else {
                          response ={
                              "msg": "Error  usuario no encontrado"
                        };
                        res.status(404);
                      }
                }
              res.send(response );
            }

    )
  }
);


//SIGNUP CON MLAB//
app.post("/apitechu/v2/signup",
  function(req, res){
    console.log("POST /apitechu/v2/signup");

    var email = req.body.Email;

    var query = 'q={"Email":"' + email + '"}';
    var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
      function(err, resMlab, body) {
          //err:400 es un error del servidor//
          //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
          //body , arrary de la tabla//

          //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
          //var response = !err ? body : {
          //  "msg": "Error obteniendo usuario."

          var response = {};

          if (err) {
            response = {
              "msg": "Error realizando el signup del usuario"
            }
            res.status(500);

          } else {
            if (body.length === 0) {
              //response =body;
              var query_id = 's={"id":-1}&l=1';
              var id = 0;

              httpClient.get("Usuarios?" + query_id + "&" + mlabApkikey,
                function(err1, resMlab1, body1) {
                  id = body1[0].id + 1;
                  console.log(id);

                  var newUser = {
                    "id": id,
                    "Email": req.body.Email,
                    "Password" : req.body.Password,
                    "Nombre": req.body.Nombre,
                    "Apellido1": req.body.Apellido1,
                    "Apellido2": req.body.Apellido2,
                    "Sexo": req.body.Sexo,
                    "Avatar": req.body.Avatar,
                    "Edad": req.body.Edad,
                    "Direccion": req.body.Direccion
                  };
                  console.log(newUser)

                  var putBody = '{"id":'+newUser.id+',"Email":"'+newUser.Email+'","Password":"'+newUser.Password+
                        '","Nombre":"'+newUser.Nombre+'","Apellido1":"'+newUser.Apellido1+'","Apellido2":"'+newUser.Apellido2+
                        '","Sexo":"'+newUser.Sexo+'","Avatar":"'+newUser.Avatar+
                        '","Edad":'+newUser.Edad+',"Direccion":"'+newUser.Direccion +'"}'

                  httpClient.post("Usuarios?" + mlabApkikey, JSON.parse(putBody));
                  console.log("post lanzado");
                  response ={
                    "msg": "Usuario dado de alta",
                    "id" : newUser.id
                  };
                  res.status(200);
                  res.send(response);
                }
              )
            }
            else {
              console.log("El usuario ya existe");
              response = {
                "msg": "Error! El usuario ya existe"
              }
              res.status(404);
            }
          }
      }
    )
  }
);

//Update Perfil CON MLAB//
app.put("/apitechu/v2/perfil",
  function(req, res){
    console.log("PUT /apitechu/v2/perfil");

    var email = req.body.Email;

    var query = 'q={"Email":"' + email + '"}';
    var httpClient= requestJson.createClient(baseMlabURL);

    httpClient.get("Usuarios?" + query + "&" + mlabApkikey,
      function(err, resMlab, body) {
          //err:400 es un error del servidor//
          //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
          //body , arrary de la tabla//

          //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
          //var response = !err ? body : {
          //  "msg": "Error obteniendo usuario."

          var response = {};

          if (err) {
            response = {
              "msg": "Error realizando el signup del usuario"
            }
            res.status(500);

          } else {
            if (body.length > 0) {
              //response =body;
              var newUser = {
                "id": req.body.id,
                "Email": req.body.Email,
                "Password" : req.body.Password,
                "Nombre": req.body.Nombre,
                "Apellido1": req.body.Apellido1,
                "Apellido2": req.body.Apellido2,
                "Sexo": req.body.Sexo,
                "Avatar": req.body.Avatar,
                "Edad": req.body.Edad,
                "Direccion": req.body.Direccion
              };
              console.log(newUser)

              var putBody = '{"id":'+newUser.id+',"Email":"'+newUser.Email+'","Password":"'+newUser.Password+
                    '","Nombre":"'+newUser.Nombre+'","Apellido1":"'+newUser.Apellido1+'","Apellido2":"'+newUser.Apellido2+
                    '","Sexo":"'+newUser.Sexo+'","Avatar":"'+newUser.Avatar+
                    '","Edad":'+newUser.Edad+',"Direccion":"'+newUser.Direccion+'"}'

              var query_id = 'q={"id":' + newUser.id + '}';

              httpClient.put("Usuarios?" + query_id + "&" + mlabApkikey, JSON.parse(putBody));
              console.log("put lanzado");

              response ={
                "msg": "Usuario actualizado",
                "id" : newUser.id
              };
              res.status(200);
              res.send(response);
            }
            else {
              console.log("Error al insertar el usuario");
              response = {
                "msg": "Error!"
              }
              res.status(404);
            }
          }
      }
    )
  }
);


/*Alta de Movimientos Traspaso a la Cuenta Origen*/
app.put("/apitechu/v2/Transferencia",
function(req, res){
  console.log("PUT /apitechu/v2/Transferencia");

  var id = req.body.id;
  var idCuenta = req.body.idCuenta;
  var idMovimiento = req.body.idMovimiento;

  var query =  'q={"id":' + id + '}';

  var httpClient= requestJson.createClient(baseMlabURL);

  /*Se obtienen todos los movimientos de esa cuenta*/
  httpClient.get("Cuentas?" + query + "&" + mlabApkikey,
    function(err, resMlab, body) {
      //err:400 es un error del servidor//
      //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
      //body , arrary de la tabla//

      //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
      //var response = !err ? body : {
      //  "msg": "Error obteniendo usuario."
      var response = {};

      if (err) {
        response = {
          "msg": "Error realizando el alta de movimiento"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          //ha recuperado el array de Movimientos;
          console.log("Entro Else");
          console.log(body[0]);
          var newMov = {
            "id": req.body.id,
            "idCuenta": req.body.idCuenta,
            "Fecha": req.body.Fecha,
            "Cuenta": req.body.Cuenta,
            "idMovimiento" : body[0].Cuentas[req.body.idCuenta-1].Movimientos.length+1,
            "Tipo": req.body.Tipo,
            "Signo": req.body.Signo,
            "Importe": req.body.Importe,
            "Longitud": req.body.Longitud,
            "Latitud": req.body.Latitud
          };
          body[0].Cuentas[req.body.idCuenta-1].Movimientos[newMov.idMovimiento-1] = {};
          console.log("prueba");
          console.log(newMov);
          body[0].Cuentas[req.body.idCuenta-1].Movimientos[newMov.idMovimiento-1].idMovimiento = newMov.idMovimiento;
          body[0].Cuentas[req.body.idCuenta-1].Movimientos[newMov.idMovimiento-1].Fecha = newMov.Fecha;
          body[0].Cuentas[req.body.idCuenta-1].Movimientos[newMov.idMovimiento-1].Tipo = newMov.Tipo;
          body[0].Cuentas[req.body.idCuenta-1].Movimientos[newMov.idMovimiento-1].Signo = newMov.Signo;
          body[0].Cuentas[req.body.idCuenta-1].Movimientos[newMov.idMovimiento-1].Importe = newMov.Importe;
          body[0].Cuentas[req.body.idCuenta-1].Movimientos[newMov.idMovimiento-1].Longitud = newMov.Longitud;
          body[0].Cuentas[req.body.idCuenta-1].Movimientos[newMov.idMovimiento-1].Latitud = newMov.Latitud;

          var query1 =  'q={"id":' + id + ', "Cuentas.idCuenta":' + idCuenta + '}';

          httpClient.put("Cuentas?" + query1 + "&" + mlabApkikey, body);
          response ={
            "msg": "Movimiento insertado",
            "idMovimiento" : newMov.idMovimiento
          };
          res.status(200);
          res.send(response);
        }
        else {
          console.log("Error al insertar el movimiento orien");
          response = {
            "msg": "Error!"
          }
          res.status(404);

        }
      }
    })
  });

  /*Alta de Movimientos Traspaso a la Cuenta Origen*/
  app.put("/apitechu/v2/Traspaso",
  function(req, res){
    console.log("PUT /apitechu/v2/Traspaso");

    var id = req.body[0].id;
    var idCuenta = req.body[0].idCuenta;
    var idMovimiento = req.body[0].idMovimiento;

    var query =  'q={"id":' + id + '}';

    var httpClient= requestJson.createClient(baseMlabURL);

    /*Se obtienen todos los movimientos de esa cuenta*/
    httpClient.get("Cuentas?" + query + "&" + mlabApkikey,
      function(err, resMlab, body) {
        //err:400 es un error del servidor//
        //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
        //body , arrary de la tabla//

        //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
        //var response = !err ? body : {
        //  "msg": "Error obteniendo usuario."
        var response = {};

        if (err) {
          response = {
            "msg": "Error realizando el alta de movimiento"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            //ha recuperado el array de Movimientos;
            console.log("Movimiento Origen");
            console.log(body[0]);
            var ori = req.body[0];
            var newMov = {
              "id": ori.id,
              "idCuenta": ori.idCuenta,
              "Fecha": ori.Fecha,
              "Cuenta": ori.Cuenta,
              "idMovimiento" : body[0].Cuentas[ori.idCuenta-1].Movimientos.length+1,
              "Tipo": ori.Tipo,
              "Signo": ori.Signo,
              "Importe": ori.Importe,
              "Longitud": ori.Longitud,
              "Latitud": ori.Latitud
            };
            body[0].Cuentas[ori.idCuenta-1].Movimientos[newMov.idMovimiento-1] = {};
            console.log(newMov);
            body[0].Cuentas[ori.idCuenta-1].Movimientos[newMov.idMovimiento-1].idMovimiento = newMov.idMovimiento;
            body[0].Cuentas[ori.idCuenta-1].Movimientos[newMov.idMovimiento-1].Fecha = newMov.Fecha;
            body[0].Cuentas[ori.idCuenta-1].Movimientos[newMov.idMovimiento-1].Tipo = newMov.Tipo;
            body[0].Cuentas[ori.idCuenta-1].Movimientos[newMov.idMovimiento-1].Signo = newMov.Signo;
            body[0].Cuentas[ori.idCuenta-1].Movimientos[newMov.idMovimiento-1].Importe = newMov.Importe;
            body[0].Cuentas[ori.idCuenta-1].Movimientos[newMov.idMovimiento-1].Longitud = newMov.Longitud;
            body[0].Cuentas[ori.idCuenta-1].Movimientos[newMov.idMovimiento-1].Latitud = newMov.Latitud;

            var query_ori =  'q={"id":' + id + ', "Cuentas.idCuenta":' + idCuenta + '}';

            httpClient.put("Cuentas?" + query_ori + "&" + mlabApkikey, body).then(function(res_ori) {
              // Get the response body (JSON parsed or jQuery object for XMLs)
              /*Se obtienen todos los movimientos de esa cuenta*/
              var id_des = req.body[1].id;
              var idCuenta_des = req.body[1].idCuenta;
              var idMovimiento_des = req.body[1].idMovimiento;

              var query_des =  'q={"id":' + id_des + '}';
              httpClient.get("Cuentas?" + query_des + "&" + mlabApkikey,
                function(err_des, resMlab_des, body_des) {
                  //err:400 es un error del servidor//
                  //resMla, Ttoda la respuesta que monta el cliente con las cabeceras y Body
                  //body , arrary de la tabla//

                  //!err ? body:, SI NO HAY UN ERROR VA A SACAR EL BODY//
                  //var response = !err ? body : {
                  //  "msg": "Error obteniendo usuario."
                  var response = {};

                  if (err_des) {
                    response = {
                      "msg": "Error realizando el alta de movimiento"
                    }
                    res.status(500);
                  } else {
                    if (body_des.length > 0) {
                      //ha recuperado el array de Movimientos;
                      console.log("Movimiento Destino");
                      console.log(body_des[0]);
                      //console.log(body[0].Cuentas[req.body.idCuenta-1].Movimientos);
                      var des = req.body[1];
                      var newMov_des = {
                        "id": des.id,
                        "idCuenta": des.idCuenta,
                        "Fecha": des.Fecha,
                        "Cuenta": des.Cuenta,
                        "idMovimiento" : body_des[0].Cuentas[des.idCuenta-1].Movimientos.length+1,
                        "Tipo": des.Tipo,
                        "Signo": des.Signo,
                        "Importe": des.Importe,
                        "Longitud": des.Longitud,
                        "Latitud": des.Latitud
                      };
                      body_des[0].Cuentas[des.idCuenta-1].Movimientos[newMov_des.idMovimiento-1] = {};
                      console.log(newMov_des);
                      body_des[0].Cuentas[des.idCuenta-1].Movimientos[newMov_des.idMovimiento-1].idMovimiento = newMov_des.idMovimiento;
                      body_des[0].Cuentas[des.idCuenta-1].Movimientos[newMov_des.idMovimiento-1].Fecha = newMov_des.Fecha;
                      body_des[0].Cuentas[des.idCuenta-1].Movimientos[newMov_des.idMovimiento-1].Tipo = newMov_des.Tipo;
                      body_des[0].Cuentas[des.idCuenta-1].Movimientos[newMov_des.idMovimiento-1].Signo = newMov_des.Signo;
                      body_des[0].Cuentas[des.idCuenta-1].Movimientos[newMov_des.idMovimiento-1].Importe = newMov_des.Importe;
                      body_des[0].Cuentas[des.idCuenta-1].Movimientos[newMov_des.idMovimiento-1].Longitud = newMov_des.Longitud;
                      body_des[0].Cuentas[des.idCuenta-1].Movimientos[newMov_des.idMovimiento-1].Latitud = newMov_des.Latitud;

                      var query_des =  'q={"id":' + id_des + ', "Cuentas.idCuenta":' + idCuenta_des + '}';

                      httpClient.put("Cuentas?" + query_des+ "&" + mlabApkikey, body_des);
                    }
                  }
                }
              );
            });

            response ={
              "msg": "Movimiento insertado",
              "idMovimiento" : newMov.idMovimiento
            };
            res.status(200);
            res.send(response);
          }
          else {
            console.log("Error al insertar el movimiento orien");
            response = {
              "msg": "Error!"
            }
            res.status(404);

          }
        }
      })
    });
